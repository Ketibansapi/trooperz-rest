# Profilizer

> An amazing web application for creating profile and start social networking


## Quick Start


```bash
# Install server dependencies
npm install

# Install client dependencies
cd client
npm install

# Run both Express & React from root
npm run dev

```

### License

This project is licensed under the MIT License
